#include <memory>
#include <stdexcept>
#include <sstream>
#include "../model/BoardModel.hpp"

/**
 * Constructor.
 * <p>
 * Initializes the tile type grid to be sized as getGridHeight() and getGridWidth() indicate.
 */
BoardModel::BoardModel() : playerHasKey(false) {
    for (int row = 0; row < getHeight(); row++) {
        std::vector<TileType> tileRow;
        for (int column = 0; column < getWidth(); column++) {
            tileRow.push_back(TileType::Empty);
        }
        this->tileType.push_back(tileRow);
    }
}

/**
 * Destructor.
 */
BoardModel::~BoardModel() {
    clearBoardEntities();
}

/**
 * Get the level number of the currently loaded level.
 * @return uint8_t
 */
uint8_t BoardModel::getLevelNum() const {
    return level;
}

/**
 * Set the number of the currently loaded level.
 * @param level
 */
void BoardModel::setLevelNum(uint8_t level) {
    this->level = level;
}

/**
 * Does the provided hit box intersect with a wall?
 * @param hitBox
 * @return boolean
 */
bool BoardModel::isHitBoxInWall(HitBox* hitBox) {
    bool isInWall = false;
    std::set<TileCoordinate> intersectingTiles = getTilesIntersectingHitBox(hitBox);
    for (const TileCoordinate& tileCoordinate : intersectingTiles) {
        if (getTileType(tileCoordinate) == TileType::Wall) {
            if (hitBox->getIntersectionPercentage(tileCoordinate) > 0) {
                isInWall = true;
                break;
            }
        }
    }
    return isInWall;
}

/**
 * Search through the list of loaded board entities and find the ones that match the provided type.
 * @param type
 * @return std::vector<BoardEntity*>
 */
std::vector<BoardEntity*> BoardModel::getEntityByType(BoardEntityType type) const {
    std::vector<BoardEntity*> entityVector;
    for (BoardEntity* entity : getBoardEntities()) {
        if (entity->getType() == type) {
            entityVector.push_back(entity);
            break;
        }
    }

    return entityVector;
}

/**
 * Change the tile type of the tile at the provided tile coordinate.
 * <p>
 * Performs a screen edge wrap around transformation on the provided tile coordinate. If the coordinate is "off screen",
 * it is mapped to a coordinate which is on screen.
 * @param tileCoordinate
 * @param newTileType
 */
void BoardModel::changeTileType(TileCoordinate tileCoordinate, TileType newTileType) {
    tileCoordinate.wrapAround(getWidth(), getHeight());
    tileType[tileCoordinate.getY()][tileCoordinate.getX()] = newTileType;
}

/**
 * Get the type of the tile at the provided coordinate.
 * <p>
 * Performs a screen edge wrap around transformation on the provided tile coordinate. If the coordinate is "off screen",
 * it is mapped to a coordinate which is on screen.
 * @param tileCoordinate
 * @return TileType
 */
TileType BoardModel::getTileType(TileCoordinate tileCoordinate) const {
    tileCoordinate.wrapAround(getWidth(), getHeight());
    return tileType[tileCoordinate.getY()][tileCoordinate.getX()];
}

/**
 * Is the tile containing the provided hit box a door?
 * @param coord
 * @return boolean
 */
bool BoardModel::isHitBoxInDoor(HitBox* hitBox) const {
    bool isInDoor = false;
    std::set<TileCoordinate> intersectingTiles = getTilesIntersectingHitBox(hitBox);
    for (const TileCoordinate& tileCoordinate : intersectingTiles) {
        if (getTileType(tileCoordinate) == TileType::Door) {
            if (hitBox->getIntersectionPercentage(tileCoordinate) > INTERSECTION_PERCENTAGE_DOOR) {
                isInDoor = true;
                break;
            }
        }
    }
    return isInDoor;
}

/**
 * Return true if the tile coordinate has a conveyor tile type.
 * @param coord
 * @return boolean
 */
bool BoardModel::isConveyor(TileCoordinate tileCoordinate) const {
    TileType tileType = getTileType(std::move(tileCoordinate));
    return (tileType == TileType::ConveyorUp || tileType == TileType::ConveyorDown
            || tileType == TileType::ConveyorRight || tileType == TileType::ConveyorLeft);
}

/**
 * Return the direction the conveyor is pointing towards.
 * @param coord
 * @return Direction
 * @throws std::invalid_argument if tile coordinate is not for conveyor tile
 */
Direction BoardModel::getConveyorDirection(TileCoordinate tileCoordinate) const {
    TileType tileType = getTileType(std::move(tileCoordinate));
    if (tileType == TileType::ConveyorUp) {
        return DIRECTION_UP;
    } else if (tileType == TileType::ConveyorDown) {
        return DIRECTION_DOWN;
    } else if (tileType == TileType::ConveyorRight) {
        return DIRECTION_RIGHT;
    } else if (tileType == TileType::ConveyorLeft) {
        return DIRECTION_LEFT;
    }

    std::stringstream errorMessage;
    errorMessage << "Non-conveyor tile queried for direction.";
    throw std::invalid_argument(errorMessage.str());
}

/**
 * Given the TileCoordinate of a teleporter tile, return the matching teleporter tile.
 * @param teleporterCoordinate
 * @return TileCoordinate
 */
TileCoordinate BoardModel::getMatchingTeleporter(TileCoordinate teleporterCoordinate) const {
    TileCoordinate matchingCoordinate = teleporterCoordinate;
    TileType teleporterType = getTileType(teleporterCoordinate);

    if (teleporterType == TileType::TeleporterRed || teleporterType == TileType::TeleporterGreen
        || teleporterType == TileType::TeleporterBlue) {
        for (int row = 0; row < getHeight(); row++) {
            for (int column = 0; column < getWidth(); column++) {
                TileCoordinate possibleMatchingCoordinate(column, row);
                if (possibleMatchingCoordinate != teleporterCoordinate) {
                    TileType possibleMatchType = getTileType(possibleMatchingCoordinate);
                    if (possibleMatchType == teleporterType) {
                        matchingCoordinate = possibleMatchingCoordinate;
                        break;
                    }
                }
            }

            if (matchingCoordinate != teleporterCoordinate) {
                break;
            }
        }
    }

    return matchingCoordinate;
}

/**
 * Get the Board Height.
 * @return uint16_t
 */
uint16_t BoardModel::getHeight() const {
    return 16;
}

/**
 * Get the Board Width.
 * @return uint16_t
 */
uint16_t BoardModel::getWidth() const {
    return 25;
}

/**
 * Get the list of loaded board entities.
 * @return std::vector<BoardEntity*>
 */
std::vector<BoardEntity*> BoardModel::getBoardEntities() const {
    return this->boardEntities;
}

/**
 * Search through the list of board entities and return the one that is the player.
 * <p>
 * It is a logic error if there is not a player.
 * @return
 */
BoardEntity* BoardModel::getPlayer() const {
    auto matchingEntityVector = getEntityByType(PLAYER);
    if (matchingEntityVector.empty()) {
        throw std::logic_error("No player found on board");
    }
    return matchingEntityVector.front();
}

/**
 * Search through the list of board entities and return the one that is the key.
 * @return
 */
BoardEntity* BoardModel::getKey() const {
    auto matchingEntityVector = getEntityByType(KEY);
    return matchingEntityVector.front();
}

/**
 * Get the coordinate $step pixels in the provided direction.
 * @param startingCoordinate
 * @param direction
 * @param step
 * @return Coordinate
 */
Coordinate BoardModel::getCoordinateInDirection(Coordinate startingCoordinate, Direction direction, int step) const {
    Coordinate newCoordinate{0, 0};

    if (direction == DIRECTION_UP) {
        newCoordinate = {startingCoordinate.getX(), startingCoordinate.getY() - step};
    }

    if (direction == DIRECTION_DOWN) {
        newCoordinate = {startingCoordinate.getX(), startingCoordinate.getY() + step};
    }

    if (direction == DIRECTION_LEFT) {
        newCoordinate = {startingCoordinate.getX() - step, startingCoordinate.getY()};
    }

    if (direction == DIRECTION_RIGHT) {
        newCoordinate = {startingCoordinate.getX() + step, startingCoordinate.getY()};
    }

    return newCoordinate;
}

/**
 * Replace the list of BoardEntity objects currently on the board.
 * @param entities
 */
void BoardModel::setBoardEntities(std::vector<BoardEntity*> entities) {
    clearBoardEntities();
    this->boardEntities = std::move(entities);
}

/**
 * Set if the player has the key.
 * @param playerHasKey
 */
void BoardModel::setPlayerHasKey(bool playerHasKey) {
    this->playerHasKey = playerHasKey;
}

/**
 * Return whether or not the player current has the key.
 * @return boolean
 */
bool BoardModel::getPlayerHasKey() const {
    return playerHasKey;
}

/**
 * Clears the list of loaded board entities and frees the memory associated with each.
 */
void BoardModel::clearBoardEntities() {
    for (BoardEntity* entity : getBoardEntities()) {
        delete entity;
    }

    boardEntities.clear();
}

/**
 * Determine if the provided TileCoordinate's tile is a teleporter.
 * @param coordinate
 * @return boolean
 */
bool BoardModel::isTeleporterTile(const TileCoordinate& coordinate) {
    TileType tileType = getTileType(coordinate);
    return    (tileType == TileType::TeleporterRed
               || tileType == TileType::TeleporterGreen
               || tileType == TileType::TeleporterBlue);
}

/**
 * Get the set of TileCoordinates which intersect with the given hit box.
 * @return std::set<TileCoordinate>
 */
std::set<TileCoordinate> BoardModel::getTilesIntersectingHitBox(HitBox* hitBox) const {
    std::set<TileCoordinate> tilesHoldingPlayer;

    for (const TileCoordinate& tileCoordinate : hitBox->getIntersectingTileCoordinates(getWidth(), getHeight())) {
        tilesHoldingPlayer.insert(tileCoordinate);
    }

    return tilesHoldingPlayer;
}


