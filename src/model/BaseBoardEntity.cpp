#include <memory>
#include "../model/BaseBoardEntity.hpp"

/**
 * Constructor.
 */
BaseBoardEntity::BaseBoardEntity(Coordinate coordinate, BoardEntityType type)
        : coordinate{std::move(coordinate)}, type{type}, groundHitBox(nullptr) {}

/**
 * Destructor.
 */
BaseBoardEntity::~BaseBoardEntity() {
    delete groundHitBox;
}

/**
 * Retrieves the hit boxes associated with the BoardEntity.
 * @return std::vector<HitBox*>
 */
HitBox* BaseBoardEntity::getGroundHitBox() {
    return groundHitBox;
}

void BaseBoardEntity::setGroundHitBox(HitBox* newGroundHitBox) {
    if (groundHitBox != nullptr) {
        delete groundHitBox;
    }

    groundHitBox = newGroundHitBox->clone();
}

/**
 * Set the screen location of the BoardEntity.
 * <p>
 * This is the coordinate of the top left corner of the board entity as it appears on the screen.
 * @param coordinate
 */
void BaseBoardEntity::setCoordinate(Coordinate coordinate) {
    this->coordinate = coordinate;

    groundHitBox->setAnchor(coordinate);
}

/**
 * Get the type of board entity.
 * @return BoardEntityType
 */
BoardEntityType BaseBoardEntity::getType() {
    return type;
}

/**
 * Get the screen location of the BoardEntity.
 * <p>
 * This is the coordinate of the top left corner of the board entity as it appears on the screen.
 * @return Coordinate
 */
Coordinate BaseBoardEntity::getCoordinate() {
    return this->coordinate;
}

/**
 * Check to see if the provided coordinate falls within the bounds of the board entity.
 * <p>
 * Internally, this merely checks all the hit boxes to see if the coordinate intersects with any of them.
 * @param coordinate
 * @return boolean
 */
bool BaseBoardEntity::intersectsWithCoordinate(Coordinate coordinate) {
    return groundHitBox->contains(coordinate);
}

/**
 * Check to see if the provided board entity intersects with this one.
 * @param entity
 * @return boolean
 */
bool BaseBoardEntity::intersectsWithEntity(BoardEntity* entity) {
    return entity->getGroundHitBox()->intersects(getGroundHitBox());
}

/**
 * Set the type of board entity.
 * @param type
 */
void BaseBoardEntity::setType(BoardEntityType type) {
    this->type = type;
}

