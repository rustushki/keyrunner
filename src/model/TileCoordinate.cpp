#include "../model/TileCoordinate.hpp"

/**
 * Default constructor.
 */
TileCoordinate::TileCoordinate() : Coordinate(0, 0) {}

/**
 * Constructor.
 * @param x
 * @param y
 */
TileCoordinate::TileCoordinate(long x, long y) : Coordinate(x, y) {}

/**
 * Convert the coordinate into a TileCoordinate.
 * @return
 */
TileCoordinate::TileCoordinate(Coordinate coordinate) : Coordinate(
        coordinate.getX() / TileCoordinate::SIZE,
        coordinate.getY() / TileCoordinate::SIZE
) {}

/**
 * Convert the TileCoordinate into a coordinate.
 * <p>
 * This is really just the coordinate of the top left corner of the tile.
 * @return Coordinate
 */
Coordinate TileCoordinate::toCoordinate() const {
    return {getX() * SIZE, getY() * SIZE};
}

/**
 * Transform this coordinate so that if either the X or Y fall outside the regions of the provided width and height,
 * they are then wrapped around to the opposite edges. The upper left corner is (0,0), the bottom right corner is
 * (boardWidth, boardHeight).
 * @param boardWidth
 * @param boardHeight
 */
void TileCoordinate::wrapAround(long boardWidth, long boardHeight) {
    if (getX() >= boardWidth) {
        setX(getX() - boardWidth);
    }

    if (getY() >= boardHeight) {
        setY(getY() - boardHeight);
    }

    if (getX() < 0) {
        setX(getX() + boardWidth);
    }

    if (getY() < 0) {
        setY(getY() + boardHeight);
    }
}

