#include <memory>
#include "../model/RectangleHitBox.hpp"
#include "../model/TileCoordinate.hpp"
#include "RectangleHitBox.hpp"


/**
 * Constructor.
 * @param anchor
 * @param width
 * @param height
 */
RectangleHitBox::RectangleHitBox(Coordinate anchor, long width, long height) : anchor(std::move(anchor)), width(width),
        height(height) {}

/**
 * Constructor.
 * @param x1 upper left x coordinate
 * @param y1 upper left y coordinate
 * @param x2 lower right x coordinate
 * @param y2 lower right y coordinate
 */
RectangleHitBox::RectangleHitBox(long x1, long y1, long x2, long y2) : anchor(Coordinate(x1, y1)), width(x2 - x1),
        height(y2 - y1) {}

/**
 * Clone idiom.
 * <p>
 * Like a copy constructor, but more flexible because it can be virtual.
 * @return
 */
RectangleHitBox* RectangleHitBox::clone() const {
    return new RectangleHitBox(getAnchor(), getWidth(), getHeight());
}

/**
 * Check to see if this hit box intersects with the provided.
 * @param hitBox
 * @return boolean
 */
bool RectangleHitBox::intersects(HitBox* hitBox) const {
    bool intersects = false;
    if (hitBox->getType() == RECTANGLE) {
        auto that = dynamic_cast<RectangleHitBox*>(hitBox);
        if (that->getLeft() < this->getRight() && that->getRight() > this->getLeft()) {
            if (that->getTop() < this->getBottom() && that->getBottom() > this->getTop()) {
                intersects = true;
            }
        }
    }
    return intersects;
}

/**
 * Check to see if the provided coordinate falls within this hit box.
 * @param coordinate
 * @return boolean
 */
bool RectangleHitBox::contains(Coordinate coordinate) const {
    bool contains = false;

    if (coordinate.getX() >= getLeft() && coordinate.getX() <= getRight()) {
        if (coordinate.getY() >= getTop() && coordinate.getY() <= getBottom()) {
            contains = true;
        }
    }

    return contains;
}

/**
 * Get the anchor point of the hit box.
 * @return Coordinate
 */
Coordinate RectangleHitBox::getAnchor() const {
    return anchor;
}

/**
 * Get the width of the hit box.
 * @return long
 */
long RectangleHitBox::getWidth() const {
    return width;
}

/**
 * Get the height of the hit box.
 * @return long
 */
long RectangleHitBox::getHeight() const {
    return height;
}

/**
 * Get the right edge of the hit box.
 * @return long
 */
long RectangleHitBox::getRight() const {
    return getAnchor().getX() + getWidth() - 1;
}

/**
 * Get the left edge of the hit box.
 * @return long
 */
long RectangleHitBox::getLeft() const {
    return getAnchor().getX();
}

/**
 * Get the top edge of the hit box.
 * @return long
 */
long RectangleHitBox::getTop() const {
    return getAnchor().getY();
}

/**
 * Get the bottom edge of the hit box.
 * @return long
 */
long RectangleHitBox::getBottom() const {
    return getAnchor().getY() + getHeight() - 1;
}

/**
 * Set the point at which this hit box is anchored.
 * <p>
 * For a rectangle hit box, the anchor is the upper left corner.
 * @param newAnchor
 */
void RectangleHitBox::setAnchor(Coordinate newAnchor) {
    this->anchor = newAnchor;
}

/**
 * Return the type of hit box.
 * <p>
 * Always returns RECTANGLE.
 * @return HitBoxType
 */
HitBoxType RectangleHitBox::getType() const {
    return RECTANGLE;
}

/**
 * Return the set of TileCoordinates which intersect with this hit box.
 * @return std::set<TileCoordinate>
 */
std::set<TileCoordinate> RectangleHitBox::getIntersectingTileCoordinates(long boardWidth, long boardHeight) const {
    std::set<TileCoordinate> intersectingTileCoordinates;
    TileCoordinate topLeft(getAnchor());
    TileCoordinate bottomRight(Coordinate(getRight(), getBottom()));

    for (long x = topLeft.getX(); x <= bottomRight.getX(); x++) {
        for (long y = topLeft.getY(); y <= bottomRight.getY(); y++) {
            TileCoordinate intersectingCoordinate(x, y);
            intersectingCoordinate.wrapAround(boardWidth, boardHeight);
            intersectingTileCoordinates.insert(intersectingCoordinate);
        }
    }

    return intersectingTileCoordinates;
}

double RectangleHitBox::getIntersectionPercentage(TileCoordinate tileCoordinate) const {
    // Compute the Intersecting Rectangle of the TileCoordinate and this HitBox.
    // [(x5, y5), (x6, y6)] are the upper left and lower right coordinates
    long x1 = tileCoordinate.toCoordinate().getX();
    long y1 = tileCoordinate.toCoordinate().getY();
    long x2 = x1 + TileCoordinate::SIZE - 1;
    long y2 = y1 + TileCoordinate::SIZE - 1;
    long x3 = getLeft();
    long y3 = getTop();
    long x4 = getRight();
    long y4 = getBottom();
    long x5 = std::max(x1, x3);
    long y5 = std::max(y1, y3);
    long x6 = std::min(x2, x4);
    long y6 = std::min(y2, y4);

    // Compute the area of the rectangle if there is an intersection; otherwise the percentage is assumed 0
    double intersectionPercentage = 0;
    if (x5 < x6 && y5 < y6) {
        long areaOfIntersectingRectangle = (x6 - x5) * (y6 - y5);
        long areaOfTileCoordinate = TileCoordinate::SIZE * TileCoordinate::SIZE;
        intersectionPercentage = (double) areaOfIntersectingRectangle / areaOfTileCoordinate;
    }

    return intersectionPercentage;
}

/**
 * Splits and wraps a hit box if its coordinates exceed the provided board width and height.
 * <p>
 * This algorithm first translates the hit box so that the hit box's upper left corner does actually fall within the
 * board width and height. Then, if both the width and height of the hit box overlap with an edge of the board, then the
 * hit box is cut into four quadrants. Each quadrant is snapped to a corner of the board. Otherwise, if the hit box only
 * overlaps one border (lower or right), the hit box is cut into two halves (vertically or horizontally). The halves are
 * snapped to their respective border.
 * <p>
 * Finally, if the hit box is not split at all, that means that it fit perfectly within the bounds of the board.
 * <p>
 * All resulting hit boxes are returned in a list of shared pointers.
 * @param boardWidth
 * @param boardHeight
 * @return the sliced hit boxes
 */
std::vector<std::shared_ptr<HitBox>> RectangleHitBox::split(long boardWidth, long boardHeight) const {
    std::vector<std::shared_ptr<HitBox>> splitHitBoxes;

    auto translated = this->wrap(boardWidth, boardHeight);
    long translatedX = translated.get()->getAnchor().getX();
    long translatedY = translated.get()->getAnchor().getY();

    // If the hit box straddles the lower right corner, cut it into quadrants
    if (translatedY + getHeight() > boardHeight && translatedX + getWidth() > boardWidth) {
        auto lr = new RectangleHitBox(translatedX, translatedY, boardWidth, boardHeight);
        splitHitBoxes.push_back(std::shared_ptr<HitBox>(lr));
        auto ul = new RectangleHitBox(0, 0, getWidth() - lr->getWidth(), getHeight() - lr->getHeight());
        splitHitBoxes.push_back(std::shared_ptr<HitBox>(ul));
        auto ll = new RectangleHitBox(0, lr->getTop(), getWidth() - lr->getWidth(), lr->getBottom());
        splitHitBoxes.push_back(std::shared_ptr<HitBox>(ll));
        auto ur = new RectangleHitBox(lr->getLeft(), ul->getTop(), lr->getRight(), ul->getBottom());
        splitHitBoxes.push_back(std::shared_ptr<HitBox>(ur));

    // If the hit box straddles the lower border, cut it horizontally
    } else if (translatedY + getHeight() > boardHeight) {
        auto dn = new RectangleHitBox(translatedX, translatedY, translatedX + getWidth(), boardHeight);
        splitHitBoxes.push_back(std::shared_ptr<HitBox>(dn));
        auto up = new RectangleHitBox(translatedX, 0, translatedX + getWidth(), getHeight() - dn->getHeight());
        splitHitBoxes.push_back(std::shared_ptr<HitBox>(up));

    // If the hit box straddles the right border, cut it vertically
    } else if (translatedX + getWidth() > boardWidth) {
        auto rt = new RectangleHitBox(translatedX, translatedY, boardWidth, translatedY + getHeight());
        splitHitBoxes.push_back(std::shared_ptr<HitBox>(rt));
        auto lf = new RectangleHitBox(0, translatedY, getWidth() - rt->getWidth(), translatedY + getHeight());
        splitHitBoxes.push_back(std::shared_ptr<HitBox>(lf));
    }

    // If no cuts have been made, return the translated hit box
    if (splitHitBoxes.empty()) {
        splitHitBoxes.push_back(translated);
    }

    return splitHitBoxes;
}

std::shared_ptr<HitBox> RectangleHitBox::wrap(long boardWidth, long boardHeight) const {
    // Translate anchor point so that it's within bounds of the board
    long translatedX = getLeft();
    while (translatedX < 0) {
        translatedX += boardWidth;
    }

    while (translatedX >= boardWidth) {
        translatedX -= boardWidth;
    }

    long translatedY = getTop();
    while (translatedY < 0) {
        translatedY += boardHeight;
    }

    while (translatedY >= boardHeight) {
        translatedY -= boardHeight;
    }

    return std::shared_ptr<HitBox>(new RectangleHitBox(Coordinate(translatedX, translatedY), getWidth(), getHeight()));
}
