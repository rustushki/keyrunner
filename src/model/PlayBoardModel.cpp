#include <set>
#include <memory>
#include <map>
#include <functional>
#include "../model/BoardModel.hpp"
#include "../model/PlayBoardModel.hpp"
#include "../model/RectangleHitBox.hpp"

PlayBoardModel::PlayBoardModel() : timeClock(0), playerSpeed(NORMAL_PLAYER_STEP) {}

/**
 * Is the currently loaded level finished?
 * <p>
 * This happens when the player has the key and is standing at the door.
 * @return boolean
 */
bool PlayBoardModel::isComplete() const {
    return (getPlayerHasKey() && isHitBoxInDoor(getPlayer()->getGroundHitBox()));
}

/**
 * Move the player in the directions set by addPlayerMovementDirection().
 */
void PlayBoardModel::movePlayer() {
    for (Direction direction : playerMovementDirections) {
        movePlayerInDirection(direction);
    }
}

/**
 * Move the player in the provided direction.
 * @param direction
 * @return true if movement occurred, false otherwise
 */
bool PlayBoardModel::movePlayerInDirection(Direction direction) {
    long boardWidth = getWidth() * TileCoordinate::SIZE;
    long boardHeight = getHeight() * TileCoordinate::SIZE;

    Coordinate oldCoordinate = getPlayer()->getCoordinate();
    Coordinate newCoordinate(0, 0);

    // Check #1: Will moving the player in the direction cause them to bump against a wall hit box?
    // If so, don't move the player. Do not continue evaluating criteria either, such as teleporters and wraparound
    // They do not apply since the player has attempted to walk into a wall. If the maximum 'step' the player can move
    // causes the player to bump into a wall, try increasingly smaller steps to see if the player would be able to move.
    for (int step = getPlayerSpeed(); step >= 0; step--) {
        if (step == 0) {
            return false;
        }

        newCoordinate = getCoordinateInDirection(getPlayerCoord(), direction, step);

        std::shared_ptr<HitBox> testingHitBox = std::shared_ptr<HitBox>(getPlayer()->getGroundHitBox()->clone());
        testingHitBox->setAnchor(newCoordinate);

        bool wallWasHit = false;
        std::vector<std::shared_ptr<HitBox>> boundarySplitHitBoxes = testingHitBox->split(boardWidth, boardHeight);
        for (const std::shared_ptr<HitBox> &hitBox : boundarySplitHitBoxes) {
            if (isHitBoxInWall(hitBox.get())) {
                wallWasHit = true;
                break;
            }
        }

        if (!wallWasHit) {
            break;
        }
    }

    // Check #2: Will moving the player in the direction cause them to move off screen?
    // If so, wrap them around to the other side(s) of the screen. It may be 2 sides if the player is walking into a
    // corner.
    if (newCoordinate.getX() >= boardWidth) {
        newCoordinate.setX(newCoordinate.getX() - boardWidth);
    } else if (newCoordinate.getX() < 0) {
        newCoordinate.setX(boardWidth + newCoordinate.getX());
    }

    if (newCoordinate.getY() >= boardHeight) {
        newCoordinate.setY(newCoordinate.getY() - boardHeight);
    } else if (newCoordinate.getY() < 0) {
        newCoordinate.setY(boardHeight + newCoordinate.getY());
    }

    // Move the player to the new coordinate (updates hit boxes too)
    setPlayerCoord(newCoordinate);

    return oldCoordinate != newCoordinate;
}

/**
 * Get the time remaining on the time clock.
 * @return long
 */
long PlayBoardModel::getTimeClock() {
    return timeClock;
}

/**
 * Set the time remaining on the time clock.
 * @param uint16_t
 */
void PlayBoardModel::setTimeClock(long timeClockValue) {
    timeClock = timeClockValue;
}

/**
 * Decrement the time clock by a certain amount.
 * @param step
 */
void PlayBoardModel::decrementTimeClock(long step) {
    timeClock -= step;
}

/**
 * Increment the time clock by a certain amount.
 * @param step
 */
void PlayBoardModel::incrementTimeClock(long step) {
    timeClock += step;
}

/**
 * Convey the player if they're on a conveyor belt.
 * <p>
 * The logic works as follows:
 *  (1) do not move the player if they're not on a conveyor belt
 *  (2) if the player is on one or more conveyor belt tiles:
 *     (a) move the in the direction of the tile which the intersect with the most
 *     (b) if movement fails (because they're blocked by an obstacle), try another conveyor instead
 */
void PlayBoardModel::conveyPlayer() {
    std::map<TileCoordinate, double> intersectionMap;
    HitBox* groundHitBox = getPlayer()->getGroundHitBox();
    auto rectangleHitBox = dynamic_cast<RectangleHitBox*>(groundHitBox);
    for (const auto& tileCoordinate : getTilesIntersectingHitBox(groundHitBox)) {
        if (isConveyor(tileCoordinate)) {
            double intersection = rectangleHitBox->getIntersectionPercentage(tileCoordinate);
            intersectionMap[tileCoordinate] = intersection;
        }
    }

    typedef std::pair<TileCoordinate, double> Pair;
    typedef std::function<bool(Pair, Pair)> Comparator;
    Comparator comparator = [](Pair elem1 , Pair elem2) {
        return elem1.second > elem2.second;
    };
	std::set<Pair, Comparator> sortedSet(intersectionMap.begin(), intersectionMap.end(), comparator);

	for (auto pair : sortedSet) {
	    TileCoordinate& tileCoordinate = pair.first;
        if (movePlayerInDirection(getConveyorDirection(tileCoordinate))) {
            break;
        }
	}
}

/**
 * Is the player allowed to use the teleporter partner tile now?
 * <p>
 * This is here so that the player has a chance to step off the teleporter tile before it can be used again.
 * @return
 */
bool PlayBoardModel::isPlayerCanUseTeleporterPartner() const {
    return this->playerCanUseTeleporterPartner;
}

/**
 * Set whether or not the player is allowed to use the teleporter partner tile.
 * @param playerOnTeleporterPartner
 */
void PlayBoardModel::setPlayerCanUseTeleporterPartner(bool playerOnTeleporterPartner) {
    this->playerCanUseTeleporterPartner = playerOnTeleporterPartner;
}

/**
 * Get the teleporter partner tile.
 * <p>
 * This is the tile the the player was teleported to when they stepped onto a teleporter.
 * @return TileCoordinate
 */
TileCoordinate PlayBoardModel::getTeleporterPartner() const {
    return teleporterPartner;
}

/**
 * Set the teleporter partner tile.
 * <p>
 * This is the tile the the player was teleported to when they stepped onto a teleporter.
 * @param teleporterPartner
 */
void PlayBoardModel::setTeleporterPartner(TileCoordinate teleporterPartner) {
    this->teleporterPartner = std::move(teleporterPartner);

}

/**
 * Adds a direction to move the player along each time movePlayer() is called.
 * @param direction
 */
void PlayBoardModel::addPlayerMovementDirection(Direction direction) {
    playerMovementDirections.insert(direction);
}

/**
 * Removes a player movement direction so that the player is not moved when movePlayer() is called.
 * @param direction
 */
void PlayBoardModel::removePlayerMovementDirection(Direction direction) {
    playerMovementDirections.erase(direction);
}

/**
 * Sets the speed at which the player will move in each direction.
 * <p>
 * Measured in pixels.
 * @param playerSpeed
 */
void PlayBoardModel::setPlayerSpeed(uint16_t playerSpeed) {
    this->playerSpeed = playerSpeed;
}

/**
 * Gets the speed at which the player will move in each direction.
 * <p>
 * Measured in pixels.
 * @return player speed
 */
uint16_t PlayBoardModel::getPlayerSpeed() const {
    return playerSpeed;
}

/**
 * Remove all directions from the set of directions to move the player.
 */
void PlayBoardModel::clearPlayerMovementDirections() {
    playerMovementDirections.clear();
}

/**
 * Get the current coordinate of the player.
 * @return BoardEntityCoordinate
 */
Coordinate PlayBoardModel::getPlayerCoord() const {
    return getPlayer()->getCoordinate();
}

/**
 * Assign a new coordinate to the player.
 * @param coordinate
 */
void PlayBoardModel::setPlayerCoord(Coordinate coordinate) {
    getPlayer()->setCoordinate(std::move(coordinate));

    if (!getPlayerHasKey()) {
        if (getKey()->intersectsWithEntity(getPlayer())) {
            setPlayerHasKey(true);
        }
    }
}

/**
 * Check to see if the player should be teleported and potentially do so.
 */
void PlayBoardModel::teleportPlayerAfterCheck() {
    // Teleport the player (if on a teleporter tile and conditions are met)
    // By default, let's choose to reset the teleporter tile unless conditions found in the loop indicate otherwise
    bool resetTeleporterPartnerUse = true;

    HitBox* groundHitBox = getPlayer()->getGroundHitBox();
    // If the hit box is in a teleporter tile, then teleport the player if they're allowed to. If the player is not
    // in a teleporter tile anymore, allow the teleporter partner tile to be used again
    for (const TileCoordinate& tileCoordinate : groundHitBox->getIntersectingTileCoordinates(getWidth(), getHeight())) {
        if (isTeleporterTile(tileCoordinate)) {
            if (groundHitBox->getIntersectionPercentage(tileCoordinate) > INTERSECTION_PERCENTAGE_TELEPORTER) {
                if (tileCoordinate != getTeleporterPartner() || isPlayerCanUseTeleporterPartner()) {
                    TileCoordinate teleporterPartner = getMatchingTeleporter(tileCoordinate);
                    setTeleporterPartner(teleporterPartner);
                    setPlayerCanUseTeleporterPartner(false);
                    resetTeleporterPartnerUse = false;
                    setPlayerCoord(teleporterPartner.toCoordinate());
                    clearPlayerMovementDirections();
                    break;

                } else if (tileCoordinate == getTeleporterPartner()) {
                    resetTeleporterPartnerUse = false;
                }
            }
        }
    }

    // If it's been determined that the player has moved off the teleporter tile (after having been teleported there),
    // go ahead and allow the tile to be used as a teleporter again
    if (resetTeleporterPartnerUse) {
        setPlayerCanUseTeleporterPartner(true);
    }
}

