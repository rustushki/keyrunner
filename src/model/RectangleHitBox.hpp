#ifndef KEYRUNNER_RECTANGLE_HIT_BOX_HPP
#define KEYRUNNER_RECTANGLE_HIT_BOX_HPP

#include "../model/HitBox.hpp"

class RectangleHitBox : public HitBox {
public:
    explicit RectangleHitBox(Coordinate anchor, long width, long height);
    explicit RectangleHitBox(long x1, long y1, long x2, long y2);
    RectangleHitBox* clone() const override;

    bool intersects(HitBox* hitBox) const override;
    bool contains(Coordinate coordinate) const override;
    std::vector<std::shared_ptr<HitBox>> split(long boardWidth, long boardHeight) const override;
    std::shared_ptr<HitBox> wrap(long boardWidth, long boardHeight) const override;
    Coordinate getAnchor() const override;
    void setAnchor(Coordinate newAnchor) override;
    long getWidth() const;
    long getHeight() const;
    long getLeft() const;
    long getRight() const;
    long getTop() const;
    long getBottom() const;
    HitBoxType getType() const override;
    std::set<TileCoordinate> getIntersectingTileCoordinates(long boardWidth, long boardHeight) const override;
    double getIntersectionPercentage(TileCoordinate tileCoordinate) const override;

private:
    long height;
    long width;
    Coordinate anchor;
};

#endif
