#ifndef PLAY_BOARD_HPP
#define PLAY_BOARD_HPP

#include "../model/BoardModel.hpp"
#include "../model/Direction.hpp"
#include "../model/Model.hpp"
#include "../model/State.hpp"

class PlayBoardModel : public BoardModel {
public:
    const static uint16_t NORMAL_PLAYER_STEP = 3;
    const static uint16_t SLOWER_PLAYER_STEP = 1;

    PlayBoardModel();

    bool isComplete() const;
    void movePlayer();
    void addPlayerMovementDirection(Direction direction);
    void removePlayerMovementDirection(Direction direction);
    void clearPlayerMovementDirections();

    void setPlayerSpeed(uint16_t playerSpeed);
    uint16_t getPlayerSpeed() const;

    Coordinate getPlayerCoord() const;
    void setPlayerCoord(Coordinate coordinate);

    long getTimeClock();
    void setTimeClock(long timeClockValue);
    void decrementTimeClock(long step);
    void incrementTimeClock(long step);

    bool isPlayerCanUseTeleporterPartner() const;
    void setPlayerCanUseTeleporterPartner(bool playerOnTeleporterPartner);
    TileCoordinate getTeleporterPartner() const;
    void setTeleporterPartner(TileCoordinate teleporterPartner);

    void conveyPlayer();
    void teleportPlayerAfterCheck();

private:
    uint16_t playerSpeed;
    long timeClock;
    bool playerCanUseTeleporterPartner;
    TileCoordinate teleporterPartner;
    std::set<Direction> playerMovementDirections;

    bool movePlayerInDirection(Direction direction);
};

#endif
