#ifndef BOARD_HPP
#define BOARD_HPP

#include <cstdint>
#include <vector>
#include "../model/BaseModel.hpp"
#include "../model/BoardEntity.hpp"
#include "../model/Direction.hpp"
#include "../model/TileType.hpp"
#include "../model/TileCoordinate.hpp"

class BoardModel : public BaseModel {
public:
    const double INTERSECTION_PERCENTAGE_TELEPORTER = 0.50;
    const double INTERSECTION_PERCENTAGE_DOOR = 0.25;

    BoardModel();
    ~BoardModel();

    uint16_t getHeight() const;
    uint16_t getWidth() const;

    uint8_t getLevelNum() const;
    void setLevelNum(uint8_t level);

    BoardEntity* getKey() const;
    BoardEntity* getPlayer() const;

    void setPlayerHasKey(bool playerHasKey);
    bool getPlayerHasKey() const;

    bool isHitBoxInWall(HitBox* hitBox);
    bool isHitBoxInDoor(HitBox* hitBox) const;
    bool isConveyor(TileCoordinate coord) const;

    TileType getTileType(TileCoordinate tileCoordinate) const;
    void changeTileType(TileCoordinate tileCoordinate, TileType newTileType);

    Direction getConveyorDirection(TileCoordinate coord) const;

    TileCoordinate getMatchingTeleporter(TileCoordinate teleporterCoordinate) const;

    std::vector<BoardEntity*> getBoardEntities() const;

    Coordinate getCoordinateInDirection(Coordinate startingCoordinate, Direction direction, int step) const;

    void setBoardEntities(std::vector<BoardEntity*> vector);

    bool isTeleporterTile(const TileCoordinate& coordinate);

    std::set<TileCoordinate> getTilesIntersectingHitBox(HitBox* hitBox) const;

private:
    uint8_t level;
    std::vector< std::vector<TileType> > tileType;
    std::vector<BoardEntity*> boardEntities;
    bool playerHasKey;

    std::vector<BoardEntity*> getEntityByType(BoardEntityType type) const;
    void clearBoardEntities();
};

#endif
