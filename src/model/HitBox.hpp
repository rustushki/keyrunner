#ifndef HIT_BOX_HPP
#define HIT_BOX_HPP

#include <set>
#include <bits/shared_ptr.h>
#include <vector>
#include "../model/Coordinate.hpp"
#include "../model/HitBoxType.hpp"
#include "../model/TileCoordinate.hpp"

class HitBox {
public:
    virtual HitBox* clone() const = 0;
    virtual bool intersects(HitBox* hitBox) const  = 0;
    virtual bool contains(Coordinate coordinate) const = 0;
    virtual HitBoxType getType() const = 0;
    virtual Coordinate getAnchor() const = 0;
    virtual void setAnchor(Coordinate newAnchor) = 0;
    virtual std::vector<std::shared_ptr<HitBox>> split(long boardWidth, long boardHeight) const = 0;
    virtual std::shared_ptr<HitBox> wrap(long boardWidth, long boardHeight) const = 0;
    virtual std::set<TileCoordinate> getIntersectingTileCoordinates(long boardWidth, long boardHeight) const = 0;
    virtual double getIntersectionPercentage(TileCoordinate tileCoordinate) const = 0;
};

#endif
