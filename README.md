[![build status](https://gitlab.com/rustushki/keyrunner/badges/master/build.svg)](https://gitlab.com/rustushki/keyrunner/commits/master)


# Summary

Help Moschata escape certain doom. The clock is ticking and she must reach the
door with the key before it's too late. KeyRunner is a free game, available in
source code under the GPLv2 License.

![KeyRunner](https://gitlab.com/rustushki/keyrunner/raw/master/screenshots/keyrunner.png)

# Gameplay / Features

* Moschata, the Key Runner
* Doors require keys
* Colored teleporter tiles
* Wrap around edges
* Conveyor belts
* Player movement not bound to grid
* 40 built in levels and a Level Editor

# Ubuntu Build Instructions

    $ sudo apt install libsdl2-ttf-dev libsdl2-image-dev libsdl2-dev cmake
    $ git clone https://gitlab.com/rustushki/keyrunner.git
    $ cd keyrunner
    $ cmake .
    $ make
    $ sudo make install
